
import express, { Application } from "express";
import { Server as SocketIOServer } from "socket.io";
import { createServer, Server as HTTPServer } from "http";
import path from 'path';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import todoListRoutes from './api/routes/todoListRoutes';

// ENV
require('dotenv').config();

export class Server {
    private httpServer: HTTPServer;
    private app: Application;
    private io: SocketIOServer;

    private readonly DEFAULT_PORT = 5000;

    private activeSockets: string[] = [];

    constructor() {
        this.app = express();
        this.httpServer = createServer(this.app);
        this.io = new SocketIOServer(this.httpServer, {
            cors: {
                origin: '*'
            }
        });

        this.connectToMongoDb();
        this.configureApp();
        this.handleRoutes();
        this.handleSocketConnection();
    }

    private connectToMongoDb(): void {
        // Mongoose connect
        function connectDB(err: any) {
            if(err) {
            console.log('MongoDb connection error:' + err);
            } else {
            console.log('MongoDb connected!');
            }
        }
        
        mongoose.Promise = global.Promise;
        if (process.env.NODE_ENV == 'dev') {
            mongoose.connect('mongodb://root:123456@127.0.0.1:27017/todoListApi?authSource=admin', connectDB);
        } else {
            mongoose.connect('a', connectDB);
        }
    }

    private configureApp(): void {
        this.app.use(express.static(path.join(__dirname, "../public")));
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
    }

    private handleRoutes(): void {
        this.app.get("/", (req, res) => {
            res.sendFile(path.join(__dirname, "public", "index.html"));
        });
        todoListRoutes(this.app);
    }

    private handleSocketConnection(): void {
        this.io.on("connection", socket => {
            console.log('New socket connection');

            const existingSocket = this.activeSockets.find(
                existingSocket => existingSocket === socket.id
            );
        
            if (!existingSocket) {
                this.activeSockets.push(socket.id);
        
                socket.emit("update-user-list", {
                    users: this.activeSockets.filter(
                        existingSocket => existingSocket !== socket.id
                    )
                });
        
                socket.broadcast.emit("update-user-list", {
                    users: [socket.id]
                });

                socket.on("disconnect", () => {
                    this.activeSockets = this.activeSockets.filter(
                      existingSocket => existingSocket !== socket.id
                    );
                    socket.broadcast.emit("remove-user", {
                      socketId: socket.id
                    });
                });

                socket.on("call-user", (data:any) => {
                    socket.to(data.to).emit("call-made", {
                      offer: data.offer,
                      socket: socket.id
                    });
                });

                socket.on("make-answer", (data:any) => {
                    socket.to(data.to).emit("answer-made", {
                      socket: socket.id,
                      answer: data.answer
                    });
                });
            }
        });
    }

    public listen(callback: (port: number) => void): void {
        this.httpServer.listen(this.DEFAULT_PORT, () =>
            callback(this.DEFAULT_PORT)
        );
    }
}
