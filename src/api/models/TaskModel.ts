import mongoose, { Schema } from 'mongoose';

const TaskSchema = new Schema({
    name: { 
        type: String, 
        required: 'Kindly ender the name of the task' 
    },
    createdDate: { 
        type: Date, default: Date.now 
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    }
});

export default mongoose.model('Tasks', TaskSchema);
