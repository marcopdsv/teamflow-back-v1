import mongoose from 'mongoose';
import TaskModel from '../models/TaskModel';
import {Request, Response} from 'express';
import asyncHandler from 'express-async-handler';

export const listTasks = asyncHandler(
    async (req: Request, res: Response) => {
        const tasks = await TaskModel.find({});
        res.json(tasks);
    }
)

export const createTask = asyncHandler(
    async (req: Request, res: Response) => {
        const newTask = new TaskModel(req.body);
        const savedTask = await newTask.save();
        res.json(savedTask);
    }   
);
