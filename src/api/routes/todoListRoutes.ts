import {Application} from 'express';
import {
    listTasks,
    createTask
} from '../controllers/todoListControllers';

export default function(app: Application) {
    app.route('/tasks').get(listTasks);
    app.route('/tasks').post(createTask);
}
